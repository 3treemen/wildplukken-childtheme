<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

/* Available layouts are single-small, single-big, blog-grid and multi-big. */

add_filter('avf_blog_style','avia_change_category_blog_layout', 10, 2); 
function avia_change_category_blog_layout($layout, $context){
    if($context == 'archive') $layout = 'blog-grid';
    return $layout;
}

/*
* Google Analytics GA4 tracking
*/
function new_google_analytics(){
    ?>
    <!– Global site tag (gtag.js) – Google Analytics –>
    <script async src=”https://www.googletagmanager.com/gtag/js?id=G-278324898”></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag(‘js’, new Date());
    
    gtag(‘config’, ‘G-278324898’);
    </script>
    <?php
    }
    add_action(‘wp_head’, ‘new_google_analytics’, 1);